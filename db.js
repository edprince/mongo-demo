var MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://test:testpass1@ds115434.mlab.com:15434/princee3-music';

// Database Name
const dbName = 'princee3-music';


// Use connect method to connect to the server
MongoClient.connect(url, {useNewUrlParser: true}).then(async client => {
  const db = client.db(dbName);
  let result = await db.collection('users').insertOne({
    email: 'DEMO@DEMO.com',
    password: 'pass',
    admin: false
  });
  client.close();
}).catch(err => {
  console.log(err);
});


